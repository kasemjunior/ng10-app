import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent {
  sampleForm: FormGroup = this.fb.group({
    nomeCompleto: [null, Validators.required],
    dataNascimento: [null],
    email: [null, [Validators.required, Validators.email]],
    numeroAleatorio: [null, [Validators.required, Validators.min(0)]]
  });

  constructor(private fb: FormBuilder) {}

  doAction(): void {
    console.log(this.sampleForm.value);
  }
}
