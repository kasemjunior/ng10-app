import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UiFormFieldComponent } from './ui-form-field/ui-form-field.component';
import { UiFormInputDirective } from './ui-form-input/ui-form-input.directive';
import { UiFormLabelComponent } from './ui-form-label/ui-form-label.component';

@NgModule({
  imports: [CommonModule],
  declarations: [UiFormFieldComponent, UiFormLabelComponent, UiFormInputDirective],
  exports: [UiFormFieldComponent, UiFormLabelComponent, UiFormInputDirective]
})
export class UiFormFieldModule {}
