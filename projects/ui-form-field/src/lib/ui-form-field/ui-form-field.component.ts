import { ChangeDetectionStrategy, Component, ContentChild, HostBinding, OnInit } from '@angular/core';
import { UiFormInputDirective } from '../ui-form-input/ui-form-input.directive';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-form-field',
  templateUrl: './ui-form-field.component.html',
  styleUrls: ['./ui-form-field.component.scss']
})
export class UiFormFieldComponent implements OnInit {
  @ContentChild(UiFormInputDirective, { static: true }) private input?: UiFormInputDirective;

  @HostBinding('class') get styleClass() {
    return 'ui-form-field';
  }

  @HostBinding('class.ng-untouched') get untouched() {
    return this.input?.control.untouched;
  }

  @HostBinding('class.ng-touched') get touched() {
    return this.input?.control.touched;
  }

  @HostBinding('class.ng-pristine') get pristine() {
    return this.input?.control.pristine;
  }

  @HostBinding('class.ng-dirty') get dirty() {
    return this.input?.control.dirty;
  }

  @HostBinding('class.ng-valid') get valid() {
    return this.input?.control.valid;
  }

  @HostBinding('class.ng-invalid') get invalid() {
    return this.input?.control.invalid;
  }

  @HostBinding('class.ng-pending') get pending() {
    return this.input?.control.pending;
  }

  formControlName?: string;

  ngOnInit(): void {
    this.formControlName = this.input?.control.name as string;
  }
}
