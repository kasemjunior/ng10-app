import { AfterViewInit, Directive, ElementRef, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[uiFormInput]'
})
export class UiFormInputDirective implements OnInit, OnDestroy, AfterViewInit {
  formControlName?: string;

  constructor(private renderer: Renderer2, private elementRef: ElementRef, public control: NgControl) {}

  ngOnInit(): void {
    this.formControlName = this.control.name as string;
    this.renderer.setAttribute(this.elementRef.nativeElement, 'id', this.formControlName);
  }

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {}
}
