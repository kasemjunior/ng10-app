import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiFormLabelComponent } from './ui-form-label.component';

describe('UiFormFieldLabelComponent', () => {
  let component: UiFormLabelComponent;
  let fixture: ComponentFixture<UiFormLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UiFormLabelComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiFormLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
