/*
 * Public API Surface of ui-input-text
 */

export * from './lib/ui-input-text.service';
export * from './lib/ui-input-text.component';
export * from './lib/ui-input-text.module';
