import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import player from 'lottie-web';
import { LottieModule } from 'ngx-lottie';
import { UiLoadingComponent } from './ui-loading.component';
import { UiLoadingConfig } from './ui-loading.config';

// Note we need a separate function as it's required
// by the AOT compiler.
// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function playerFactory() {
  return player;
}

@NgModule({
  imports: [CommonModule, LottieModule.forRoot({ player: playerFactory })],
  declarations: [UiLoadingComponent],
  exports: [UiLoadingComponent]
})
export class UiLoadingModule {
  static forRoot(config: UiLoadingConfig): ModuleWithProviders<UiLoadingModule> {
    return {
      ngModule: UiLoadingModule,
      providers: [{ provide: UiLoadingConfig, useValue: config }]
    };
  }
}
