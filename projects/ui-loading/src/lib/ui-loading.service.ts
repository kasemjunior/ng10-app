import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiLoadingService {
  isVisible$: Observable<boolean>;

  private visible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
    this.isVisible$ = this.visible$.asObservable();
  }

  show(): void {
    this.visible$.next(true);
  }

  hide(): void {
    this.visible$.next(false);
  }
}
