import { NgModule } from '@angular/core';
import { UiModalComponent } from './ui-modal.component';

@NgModule({
  declarations: [UiModalComponent],
  imports: [],
  exports: [UiModalComponent]
})
export class UiModalModule {}
