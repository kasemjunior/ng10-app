/*
 * Public API Surface of ui-modal
 */

export * from './lib/ui-modal.service';
export * from './lib/ui-modal.component';
export * from './lib/ui-modal.module';
