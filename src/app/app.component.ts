import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UiLoadingService } from 'ui-loading';

@Component({
  selector: 'app-root',
  template: `
    <h1>{{ title }} app is running!</h1>
    <router-outlet></router-outlet>
    <ui-loading></ui-loading>
  `
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'ng10-app';

  navigationStart$: Observable<boolean> = of(false).pipe(
    switchMap(() => this.router.events),
    map((e) => e instanceof NavigationStart)
  );

  navigationEnd$: Observable<boolean> = of(false);

  private routerEventsSub?: Subscription;

  constructor(private router: Router, private loading: UiLoadingService) {}

  ngOnInit(): void {
    this.routerEventsSub = this.router.events.subscribe((e) => {
      if (e instanceof NavigationStart) {
        this.loading.show();
      } else if (e instanceof NavigationCancel) {
        this.loading.hide();
      } else if (e instanceof NavigationEnd) {
        this.loading.hide();
      } else if (e instanceof NavigationError) {
        this.loading.hide();
      }
    });
  }

  ngOnDestroy(): void {
    this.routerEventsSub?.unsubscribe();
  }
}
