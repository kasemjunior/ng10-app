import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DummyComponent } from './dummy.component';
import { DummyResolver } from './dummy.resolver';

const routes: Routes = [
  {
    path: '',
    component: DummyComponent,
    resolve: {
      fooData: DummyResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DummyRoutingModule {}
