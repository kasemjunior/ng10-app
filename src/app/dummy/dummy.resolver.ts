import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DummyResolver implements Resolve<Observable<string>> {
  resolve(): Observable<string> {
    return of('The Dummy Resolver 2000ms').pipe(delay(2000));
  }
}
